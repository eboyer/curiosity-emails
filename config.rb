activate :livereload
# set :css_dir, 'stylesheets'
# set :js_dir, 'javascripts'
set :images_dir, 'images'

page "/", :layout => "stripped"

page "/v1-server/*", :layout => "server"

page "/themeforest/*", :layout => "themeforest"

page "/newsletters/*", :layout => false

page "/v2/*", :layout => "v2"

# Build-specific configuration
configure :build do
  activate :directory_indexes

  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  activate :cache_buster

  # Use relative URLs
  activate :relative_assets

  # Compress PNGs after build
  # First: gem install middleman-smusher
  # require "middleman-smusher"
  # activate :smusher

  # Or use a different image path
  # set :http_path, "/Content/images/"
end
